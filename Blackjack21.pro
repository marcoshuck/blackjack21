#-------------------------------------------------
#
# Project created by QtCreator 2015-08-18T00:51:48
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Blackjack21
TEMPLATE = app


SOURCES += main.cpp\
        ventanaprincipal.cpp \
    ventanaunjugador.cpp \
    ucarta.cpp

HEADERS  += ventanaprincipal.h \
    ventanaunjugador.h \
    ucarta.h

FORMS    += ventanaprincipal.ui \
    ventanaunjugador.ui
