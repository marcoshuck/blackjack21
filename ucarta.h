#ifndef UCARTA_H
#define UCARTA_H

#include <QPixmap>

class uCarta
{
protected:
    unsigned int valor;
    char tipo;
    QString dir_img;
public:
    uCarta(unsigned int valor, char tipo);
    bool isTipoCarta(char tipo);
};

#endif // UCARTA_H
