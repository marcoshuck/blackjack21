#ifndef VENTANAUNJUGADOR_H
#define VENTANAUNJUGADOR_H

#include <QDialog>

namespace Ui {
class VentanaUnJugador;
}

class VentanaUnJugador : public QDialog
{
    Q_OBJECT

public:
    explicit VentanaUnJugador(QWidget *parent = 0);
    ~VentanaUnJugador();

private:
    Ui::VentanaUnJugador *ui;
};

#endif // VENTANAUNJUGADOR_H
